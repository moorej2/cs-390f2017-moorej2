# date: 29 Oct 2017

library(tidyverse)

# psych packname and tools

# install if needed?
# install.packnames("psych")

library(psych)

data("presidential")
View(presidential)
# what is this data? https://www.personality-project.org/r/html/presidents.html
describe(presidents) #basic descriptive statistics
summary(presidents) # more basic descriptive stats, different order of information

# get to know the types of the data
sapply(presidents, typeof)
str(presidents)
#basic descriptive statistics by a grouping variable (Here: separation by gender)
describeBy(presidents,presidents$name,skew=FALSE,ranges=FALSE)
#View(presidents)


print("outliers")
# Using the outlier function to graphically show outliers. The y axis is the Mahalanobis D2, the X axis is the distribution of χ2 for the same number of degrees of freedom. The outliers detected here may be shown graphically using pairs.panels (see 2, and may be found by sorting d2.)

#One way to detect unusual data is to consider how far each data point is from the multivariate centroid of the data. That is, find the squared Mahalanobis distance for each data point and then compare these to the expected values of χ2. This produces a Q-Q (quantle-quantile) plot with the n most extreme data points labeled (Figure 1, Psych overview). The outlier values are in the vector d2.

d2 <- outlier(presidents,cex=.8) # save the outliers in a table
#View(d2)
outlier(presidents,cex=.8)

#note: 
#Defining the Mahalanobis distance
#You can use the probability contours to define the Mahalanobis distance. The Mahalanobis distance has the following properties:
  
#It accounts for the fact that the variances in each direction are different.
#It accounts for the covariance between variables.
#It reduces to the familiar Euclidean distance for uncorrelated variables with unit variance.

#finding correlations: range is -1 (negative correlation) to 1 (positive correlation)

# Demo: Which columns are saying the exact same or exact opposite?
cor(c(1,2,3,4,5,6),c(1,2,3,4,5,6))
# gives a 1 for highest correlation

cor(c(1,2,3,4,5,6),c(6,5,4,3,2,1))
# gives a -1 for lowest correlation

# exact correlations as graphics
nameAndNew_perfectCorr <- mutate(presidents, new = presidents$name)
# show all plots
pairs.panels(nameAndNew_perfectCorr)

nameAndNew_perfectCorr <- mutate(presidents, new = presidents$name,negativename = -name)

# A zoom-in: see only those columns that actually correlate in some way
pairs.panels(nameAndNew_perfectCorr[c(3,7,8)])

# show all plots
pairs.panels(nameAndNew_perfectCorr)


d2 <- outlier(presidents,cex=.8)

sat.d2 <- data.frame(presidents,d2) #combine the d2 statistics from before with the presidents data.frame


#Figure 2: Using the pairs.panels function to graphically show relationships. The x axisin each scatter plot represents the column variable, the y axis the row variable. Note the extreme outlier for the ACT. If the plot character were set to a period (pch=’.’) it wouldmake a cleaner graphic, but in to show the outliers in color we use the plot characters 21 and 22.

# 21 means the circular point

#The yellow and blue are plotted depending on the truthfulness of the expression in the square brackets [d2 > 25) + 1]: true -> yellow, false -> blue
#used to show outliers
pairs.panels(presidents,bg=c("yellow","blue")[(d2 > 25)+1],pch=21)

#change the plotting point shape to squares, number 22
pairs.panels(presidents,bg=c("yellow","blue")[(d2 > 25)+1],pch=22)


# add a column called "new" which is a copy of presidents$name for positive correlation
nameAndNew_perfectCorr <- mutate(presidents, new = presidentst$name)
pairs.panels(nameAndNew_perfectCorr)


# add a column called "new" which is a copy of presidents$name for negative correlation
nameAndNew_perfectCorr <- mutate(presidents, new = presidents$name,negativename = -name)
pairs.panels(nameAndNew_perfectCorr)

#new data in psych packname: can you find the correlations?
pairs.panels(affect[c(14,15,16,17,20)])
pairs.panels(affect[c(14,15)])                      

violinBy(presidents[5:6],presidents$gender,grp.name=c("M", "F"),main="Density Plot by gender for SAT V and Q")

data("presidential")
View(presidential)
