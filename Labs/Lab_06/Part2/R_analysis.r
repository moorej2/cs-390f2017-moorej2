install.packages("dslabs")
install.packages('tidyverse', dependencies=TRUE, type="source")
install.packages("ggplot2")
library(dslabs)
library(tidyverse)
library(ggplot2)
install.packages("poliscidata")
library(poliscidata)
poliSciData <- read.table(file.choose(), header = TRUE, sep = ",")
View(poliSciData)


#Question on LSC funding
dat <- select(poliSciData, states, lscgrnts, private,year)
View(dat)
dat1 <- filter(dat, year == 1996)
View(dat1)
library(ggplot2)
ggplot(data = dat1, mapping = aes(x = state, y = lscgrnts, color = lscgrnts)) + geom_histogram(stat = "identity") + theme(axis.text.x = element_text(angle = 90, hjust = 1))

#ggplot(data = dat1, mapping = aes(x = state, y = private, color = private)) + geom_histogram(stat = "identity", color("blue")) + theme(axis.text.x = element_text(angle = 90, hjust = 1))
#ggplot(data = dat1, mapping = aes(x = state, y = lscgrbts)) + geom_point() + geom_smooth(se = FALSE) + geom_vline(xintercept = 1963)


#Question 5
dat2 <- select(poliSciData,private, year)
View(dat2)
#Trial and error
#dat2 <- filter(dat, year == 1996)
#ggplot(data = dat2, mapping = aes(x = state, y = private, color = state)) + geom_histogram(stat = "identity") + theme(axis.text.x = element_text(angle = 90, hjust = 1))
#dat2 <- filter(dat, year == 1997)
#ggplot(data = dat2, mapping = aes(x = state, y = private, color = state)) + geom_histogram(stat = "identity") + theme(axis.text.x = element_text(angle = 90, hjust = 1))
#dat2 <- filter(dat, year == 1998)
#ggplot(data = dat2, mapping = aes(x = state, y = private, color = state)) + geom_histogram(stat = "identity") + theme(axis.text.x = element_text(angle = 90, hjust = 1))
#dat2 <- filter(dat, year == 1996, 1997, 1998)
ggplot(data = dat2, mapping = aes(x = year, y = private, color = year)) + geom_histogram(stat = "identity") + theme(axis.text.x = element_text(angle = 90, hjust = 1))

#Question 4
dat3 <- select(poliSciData,lscgrnts, povrate, year)
View(dat3)
dat3 <- filter(dat, year == 1996)
ggplot(data = dat3, mapping = aes(x = povrate, y = lscgrnts, color = year)) + geom_point(stat = "identity") + theme(axis.text.x = element_text(angle = 90, hjust = 1))
geom_line(color='red',data = predicted_df, aes(x=mpg_pred, y=hp))

#Question 1
dat4 <- select(poliSciData,private, povrate, year)
View(dat4)
dat4 <- filter(dat, year == 1996)
ggplot(data = dat4, mapping = aes(x = povrate, y = private, color = year)) + geom_point(stat = "identity") + theme(axis.text.x = element_text(angle = 90, hjust = 1))

#Question 2
dat5 <- select(poliSciData, popltion, grandtot, year)
View(dat5)
ggplot(data = dat5, mapping = aes(x = popltion, y = grandtot, color = year)) + geom_point(stat = "identity") + theme(axis.text.x = element_text(angle = 90, hjust = 1))

#Question 3
dat6 <- select(poliSciData, othrppp, state, year)
View(dat6)
ggplot(data = dat6, mapping = aes(x = state, y = othrppp, color = state)) + geom_point(stat = "identity") + theme(axis.text.x = element_text(angle = 90, hjust = 1))
#Question3_2 to crosscheck for outlier
dat7 <- select(poliSciData, outlier, state, year)
View(dat7)
ggplot(data = dat7, mapping = aes(x = state, y = outlier, color = state)) + geom_boxplot(stat = "identity") + theme(axis.text.x = element_text(angle = 90, hjust = 1))
