# Name: Jeremy Moore
# Date: 22 Sept 2017


install.packages("dslabs")
install.packages("tidyverse", dependencies=TRUE, type="source")
install.packages("ggplot2")

data(us_contagious_diseases)

library(dslabs)
library(tidyverse) //this is to load dplyr tool
library(ggplot2)


Question 1
dat <- filter(us_contagious_diseases, diesease == "Measles", state != "Alaska", state != "Hawaii")
dat1 <- mutate(dat, people_rate = population/ 100000)
#Q2
dat2 <- filter(us_contagious_diseases, disease == "Measles" , state == "California")
ggplot(data = dat2, mapping = aes(x = year, y = count)) + geom_point() + geom_smooth(se = FALSE) + geom_vline(xintercept = 1963)

#Q3
dat3 <- filter(us_contagious_diseases, disease == "Measles")
ggplot(data = dat3, aes(x = year, y = count)) + geom_histogram(stat = "identity", bins = 1)
#Q4
dat4 <- mutate(dat3 , countstqr = sqrt(count) )
ggplot(data = dat4, aes(x = year, y = countstqr)) + geom_histogram(stat = "identity",bins = 1)

ggplot(data = dat4, aes(x = year, y = countstqr), yaxp  = c(0, 80000, 4)) + geom_histogram(stat = "identity",bins = 1)
#Q5
dat5 <- mutate(dat3, avgcount = count/50)
ggplot(data = dat1, aes(x= year , y=count)) + geom_boxplot ( color="blue", fill="blue", alpha=0.4, notch=TRUE, notchwidth = 0.8, outlier.colour="red", outlier.fill="red", outlier.size=3)
#Q6
ggplot(data = dat5, aes(x= state , y=avgcount)) + geom_boxplot ( color="blue", fill="blue", alpha=0.4, outlier.colour="red", outlier.fill="red", outlier.size=3)
#Q7
ggplot(data = dat4, aes(x= year , y= countstqr, color = state )) + geom_histogram (stat = "identity")
