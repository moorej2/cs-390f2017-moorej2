# date: 29 Oct 2017
#Jeremy Moore
library(tidyverse)
library(ggplot2)
library(dplyr)
library(psych)
options(stringsAsFactors = FALSE)
w <-file.choose()
CollegeDep <- read.csv(w)

View(CollegeDep)

describe(CollegeDep)
summary(CollegeDep) # summary


# Estimating a function

mod <- lm(Male_Amount ~ MaleDepRt, data = CollegeDep
mod1 <- lm(Female_Amount ~ FemaDepRt, data = CollegeDep) 
mod2 <- lm(Year ~ TotalRate, data = CollegeDep) 
mod3 <- lm(Year ~ MaleDepRt, data = CollegeDep)
mod4 <- lm(Year ~ FemaDepRt)
Year ~ TotalRate
class(Year ~ TotalRate)

mod # show the model details. Note the call and the coefficients
#more details about the model.
names(mod)
summary(mod)
predict(mod)
resid(mod)
coef(mod)
coefficients(mod)

names(mod1)
summary(mod1)
predict(mod1)
resid(mod1)
coef(mod1)
coefficients(mod1)

names(mod2)
summary(mod2)
predict(mod2)
resid(mod2)
coef(mod2)
coefficients(mod2)

names(mod3)
summary(mod3)
predict(mod3)
resid(mod3)
coef(mod3)
coefficients(mod3)

names(mod4)
summary(mod4)
predict(mod4)
resid(mod4)
coef(mod4)
coefficients(mod4)

qplot(Total, predict(mod), data = CollegeDep, geom = "line")

qplot(MaleDepRt, TotalRate, data = CollegeDep) + geom_smooth(method = lm)

qplot(FemaDepRt, TotalRate, data = CollegeDep) +geom_smooth(se = FALSE, method = lm) #shading  without 
qplot(Year, TotalRate, data = CollegeDep) +geom_smooth(se = TRUE, method = lm) # shading with


summary(mod) #summary
predict(mod) # predictions
resid(mod) # residuals

summary(mod1) #summary
predict(mod1) # predictions at original vals
resid(mod1) # residuals

summary(mod2) #summary
predict(mod2) # predictions at original vals
resid(mod2) # residuals

summary(mod3) #summary
predict(mod3) # predictions at original vals
resid(mod3) # residuals


#plot the data
CollegeDep %>% ggplot(aes(x = MaleDepRt, y = TotalRate)) + geom_point(alpha = I(1/4)) + geom_smooth(method = lm)
CollegeDep %>% ggplot(aes(x = FemaDepRt, y = TotalRate)) + geom_point(alpha = I(1/4)) + geom_smooth(method = lm)


hmod <- lm(TotalRate ~ FemaDepRt, data = CollegeDep)
hmod1 <- lm(TotalRate ~ MaleDepRt, data = CollegeDep)
hmod2 <- lm(Year ~TotalRate, data = CollegeDep)
# plot the lines
#Question 1 Is there a corellation between the Male college student depession rates and is it increasing, decreasing or 1:1?
CollegeDep %>% ggplot(aes(x = MaleDepRt, y = Male_Amount)) + geom_point(alpha = I(1/4)) + geom_smooth(method = lm)
#Question 2 Is there a corellation between the Female college student depession rates and is it increasing, decreasing or 1:1?
CollegeDep %>% ggplot(aes(x = FemaDepRt, y = Female_Amount)) + geom_point(alpha = I(1/4)) + geom_smooth(method = lm)

#Question 3 Is there a corellation between Female college student depession rates and the year?
CollegeDep %>% ggplot(aes(x = FemaDepRt, y = Year)) + geom_point(alpha = I(1/4)) + geom_smooth(method = lm)
#Question 4 Is there a corellation between Male college student depession rates and the year?
CollegeDep %>% ggplot(aes(x = MaleDepRt, y = Year)) + geom_point(alpha = I(1/4)) + geom_smooth(method = lm)
#Question 5 Is there a corellation between totals college student depession rates and the year?
CollegeDep %>% ggplot(aes(x = TotalRate, y = Year)) + geom_point(alpha = I(1/4)) + geom_smooth(method = lm)
